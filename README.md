# Parcours Numérique E3: Introduction à l'IA et au Machine Learning

## Compte tenu de l'indisponibilité de salle informatique pour une partie des créneaux dévolus aux cours/TD/Projets, veuillez, si vous le pouvez, vous munir de vous ordinateurs personnels. 


## Planning soutenance/présentation des projets
| Num Groupe  | Etudiant | Etudiant | Etudiant         | Projet |  HEURE DE PASSAGE 30/5/22
| :------- |:---------------:| :---------------:|:---------------:|:-----:|---------------------:|
| G1  |   KERINO H.        |  LEBAS M. | MARTY B. | GreenEr | 8:15-8:45
| G2  |   ELGAF A.        |  AUGEREAU B. | ADMANS L. | GreenEr | 8:50-9:20
| G3  |   BOICHAT G.        |  PEQUEUR-TARRAIL T.| MATHIEU L. | Atmo | 9:25-10:00
| G4  |   AYALA A.        |  ATHEA J. | SOUFARGI F. | GreenEr | 10:00-10:35
| G5  |   PICHON T.        |  JAILLET R. | DAI P.  | GreenEr | 10:45-11:20
| G6  |   OWANA S.        |   TAPIE G. | ** | Atmo | 11:20-11:55
| G7  |   NAYME T.        |  FOFANA Y. | ** | Atmo | 11:55-12:30


## Pour la soutenance : afin de ne pas avoir à recalculer un modèle (souvent c'est long...), vous pouvez enregistrer ces derniers et les ré-utiliser ultérieurement : voir https://www.tensorflow.org/guide/keras/save_and_serialize


## `News` : Calendrier du 12/5 au 30/5
Séances dédiées aux projets : questions/réponses : 

 **Jeudi 12/5/2022   8:00 - 12:00**

 **Lundi 16/5/2022   8:00 - 12:00**

 **Jeudi 19/5/2022   8:00 - 10:00**

 **Lundi 23/5/2022   10:00- 12:00**

### EXAMEN 'partie projet' : lundi 30/05/2022  8:00-12:00 : présentation orale des résultats (30 minutes/trinôme)
La présentation ne doit **pas excéder 15 minutes (au delà vous serez interrompus)** et  peut s'appuyer sur quelques slides ou être intégralement menée à partie d'un notebook jupyter intégrant les commentaires sous forme de cellules Markdown. 15 à 20 minutes seront reservées aux questions. 

Le code executable, sous forme de notebook jupyter, devra être rendu le jour de la sotenance, soit par mail ou via une clé USB. 
Le compte rendu de projet **n'excédera pas 10 à 12 pages**. Il devra expliquer les points sur lesquels se sont portés les efforts de développement et discuter les résultats obtenus. **Il n'est pas utile de paraphraser les documents fournis**. Il est par contre **indispensable** que les références utilisées soient indiquées de manière précise (site web : adresse+date de consultation / ouvrage : titre+auteur+editeur+année / publication scientifique : auteurs, titre, revue, num_pages, num_vomule, année). 

### EXAMEN 'partie cours' : Mercredi  1/06/2022  9:00-10:00
L'examen prendra la forme d'un QCM, durera 1 heure et protera sur l'ensemble des notions abordées lors des cours/BE.  Aucun document n'est autorisé, à l'exception d'une feuille A4 personnelle, manuscrite (pas de photocopie). Calculettes et téléphones proscrits.


<!-- (les instructions d'installation des logiciels nécessaires  sont décrites plus bas). -->

<!-- ## Le cours de lundi 28 /3/ 2022 aura lieu de 10:15 à 12:15. -->

<!-- Merci de nous indiquer
[ici](https://edition-collaborative.grenet.fr/sh/Xk3) vos ressources informatiques (laptop ?) pour suivre les séances de CTD.

- **Attention**: la séance du lundi 26/4 (8h->12h15) sera entièrement en **distanciel**. Les liens zoom sont disponibles sur la [page chamilo du parcours numérique](https://chamilo.grenoble-inp.fr/main/course_home/course_home.php?cidReq=ENSE33EU6NUM9)
- Le pad pour rédiger vos questions avant chaque cours :
http://pads.univ-grenoble-alpes.fr/p/parcoursnum_E3_2021
(nos réponses viendront en séance à l'oral)
-->
<!-- On se retouve en **présentiel** pour la séance du lundi 3/5 (8h->10h00 en salle 2B001, puis 10h15->12h15 en 3A014) qui sera consacrée aux [projets](https://chamilo.grenoble-inp.fr/main/course_home/course_home.php?cidReq=ENSE33EU6NUM9) -->


<!-- #### Mini-projet

*Les projets sont lancés !*
Deux sujets au choix vous sont proposés :
1. analyse/prédiction des données de consommation électrique
 dans le batiment Green-ER
2. analyse/prédiction des données ATMO de pollution atmosphérique (Ozone) dans la région Auvergne-Rhône-Alpes

Vous pouvez retrouver le descriptif,  les notebooks introductifs, la doc, les données, ... à partir du répertoire
[mini-project](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/mini-project/).
-->

<!-- Les ressources informatiques utiles pour exécuter/modifier/sauvegarder vos codes python (notebooks) sont rappelées [ici](#outils).-->

<!-- A remplir pour lundi 10 mai : la *répartition groupe/projet* (cf lien envoyé par mail) afin de former trois binômes/trinômes par sujet.
-->

<!-- #### Travail à préparer pour la séance du Lundi 14/5

- S vous ne l'avez pas déjà fait : Rédiger les réponses aux questions  des Notebooks sur les arbres de classification ou de regression, ainsi que les forétes aléatoires (Notebooks N2_b, N3_a, N3_b de la section [6_classification Trees]) 
- Rédiger les réponses aux questions  des Notebooks B1, N2 et N3 de la section [8_NN_Perceptron_MLP]


#### ~~Travail à préparer pour la séance du jeudi 29/4 8h00-10h00 et 10h15-12h15 (4h)~~

- ~~Lire l'introduction générale au réseau de neurones [7_intro_survol_NN.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/7_intro_survol_NN.pdf) et le début (-> slide 7) du cours sur les perceptrons [8_NN_Perceptron_MLP.pdf]((https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/8_NN_Perceptron_MLP.pdf)).~~ 
- ~~Préparer vos questions !~~
- ~~Rédiger les réponses aux questions  des Notebooks sur les arbres de classification ou de regression, ainsi que les forétes aléatoires (Notebooks N2_b, N3_a, N3_b de la section [6_classification Trees]) -à rendre au plus tard le 3/5 si manque de temps-~~

#### ~~Travail à préparer pour la séance du lundi 26/4 8h00-10h00 et 10h15-12h15 (4h)~~

- ~~finir de lire le cours (transparents) sur les arbres et l'apprentissage ensembliste [6_Trees_RandomForest_Boosting.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/6_Trees_RandomForest_Boosting.pdf), **lire jusqu'à la fin (slide 26)**~~
  - ~~Répondre aux questions sur les mesures d'impureté GINI et Entropie, des slides 13 et 14.~~
  - ~~Rédiger les réponses aux questions des notebooks correspondants N1 et N2_a. Vos réponses sont à envoyer à olivier.michel@grenoble-inp.fr en précisant le sujet : "Quizz-Notebooks-IA-1".~~
- ~~finir de lire le cours (transparents) sur la régularisation [5_linear_model_regularization.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/5_linear_model_regularization.pdf)~~
- ~~préparer vos questions !~~

#### ~~Travail à préparer pour la séance du lundi 12/4 8h00-10h00 et 10h15-12h15 (4h)~~
- ~~lire le cours (transparents) sur les modèles linéaires [4_linear_regression.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/4_linear_regression.pdf)~~
- ~~lire le cours (transparents) sur les arbres de décision [6_Trees_RandomForest_Boosting.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/6_Trees_RandomForest_Boosting.pdf), **lire jusqu'à 'Ensemble Learning' slide 17**~~
- ~~préparer vos questions !~~
--> 


#### Travail à préparer pour la séance du jeudi 12/5 (4h) :

- Vous devez constituer les trinômes pour les mni projets. 
- Chaque trinôme doit choisir un sujt par les projest ATMO ou GREENER. Une brève description en est donnée [ici] (https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/mini-project/mini-project_descr.pdf)
- Une première prise en main des données pour chaque projet se trouve sous forme de notebook jupyter pour chacun des deux projets ATMO GREENER, respectivement [ici (atmo)](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/mini-project/atmo) et [ici (greener)](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/mini-project/greener). Vous trouverez aussi dans ces répertoires, quelques fichiers d'information et de description des données. Les fichiers *'readme.md'* contiennent des informations sur les structures des données. 

Les ressources informatiques utiles pour exécuter/modifier/sauvegarder vos codes python (notebooks) sont rappelées [ici](#outils).

#### Travail à préparer pour la séance du Lundi 9/5/2022  (4h) 
- Lire les slides [8_NN_Perceptron_MLP.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/slides/8_NN_Perceptron_MLP.pdf) et [9_RNN_LSTM](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/slides/9_RNN_LSTM.pdf)
- Préparer les notebook  sur le [perceptron et les réseaux de neurones](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/slides/8_NN_Perceptron_MLP)
- préparer vos questions !

#### Travail à préparer pour la séance du Lundi 2/5/2022  (2h) 
- Travailler sur les notebook  [6_classification_trees](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/notebooks/6_classification_trees)
On portera une attention particulière au notebook [N2_b_Cost_Complexity_Pruning_Regressor.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/notebooks/6_classification_trees/N2_b_Cost_Complexity_Pruning_Regressor.ipynb)
- préparer vos questions !

#### Travail à préparer pour la séance du Lundi 25/4/2022  (2h) 
- Terminer de travailler sur les questions des BE sur la régularisation. 
- Déterminer les valeurs maximales et minimales de l'indice de Gini d'un ensemble contenant C labels différents. Pour quelle loi de probabilité sur l'ensemble des labels ces extrema sont-ils atteints? 
- Commencer à travailler sur les notebook  [6_classification_trees](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/notebooks/6_classification_trees)
- préparer vos questions !

#### Travail à préparer pour la séance du jeudi 14/4/2022  (2h) 
- Terminer de travailler sur les questions des BE sur la régularisation. 
- lire le cours (transparents) sur les méthodes de classification/régression par arbres [6_Trees_RamdomForest_Boosting.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/slides/6_Trees_RamdomForest_Boosting.pdf)
- Commencer à travailler sur les notebook  [6_classification_trees](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/notebooks/6_classification_trees)
- préparer vos questions !

#### Travail à préparer pour la séance du lundi 11/4/2022  (2h) 
- Terminer de travailler sur les questions des BE sur la régression. 
- lire le cours (transparents) sur les méthodes régularisation [5_linear_model_regularization.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/slides/5_linear_model_regularization.pdf)
- Commencer à travailler sur les notebook  [5_regularization ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/notebooks/5_regularization)
- préparer vos questions !

#### Travail à préparer pour la séance du lundi4/4/2022  (2h) 
- Terminer de travailler sur les questions des BE sur 'model_assesment" . 
- lire le cours (transparents) sur les méthodes de régressions linéaires [4_linear_regression.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/slides/4_linear_regression.pdf)
- Commencer à travailler sur les notebook  [4_regression ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/notebooks/4_regression)
- préparer vos questions !

#### Travail à préparer pour la séance du lundi 28/3/2022  (2h) 
- Terminer de travailler sur les questions des BE sur les k-PPV. 
- lire le cours (transparents) sur les méthodes de validation [3_model_assesment.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/slides/3_model_assesment.pdf)
- Commencer à travailler sur le notebook  [3_model_assesment.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/notebooks/3_model_assesment)
- préparer vos questions !

#### Travail à préparer pour la séance du lundi 21/3/2022  (2h) 
- lire le cours (transparents) sur les k plus proches voisins [2_knn.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/slides/2_knn.pdf) 
- lire le cours (transparents) sur les méthodes de validation [3_model_assesment.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/blob/master/slides/3_model_assesment.pdf)
- préparer vos questions !


#### 1er cours : 7/3/2022 Introduction. 
----


<!--

#### Mini-projet

*Les projets sont lancés !*
Deux sujets au choix vous sont proposés :
1. analyse/prédiction des données de consommation électrique
 dans le batiment Green-ER
2. analyse/prédiction des données ATMO de pollution atmosphérique (Ozone) dans la région Auvergne-Rhône-Alpes

Vous pouvez retrouver le descriptif,  les notebooks introductifs, la doc, les données, ... à partir du répertoire
[mini-project](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/mini-project/).

Les ressources informatiques utiles pour exécuter/modifier/sauvegarder vos codes python (notebooks) sont rappelées [ici](#outils).


A remplir pour jeudi 07/5 : la *répartition groupe/projet* (cf lien sur le serveur Riot) afin de former trois trinômes par sujet.



#### ~~Travail à préparer pour la séance du lundi 27/4 8h-9h~~
~~Cette séance de 1 heure sera consacrée à repondre aux questions relatives aux exercices et Notebooks des chapitres 6 à 8.~~

#### ~~Travail à préparer pour la séance de jeudi 16/4 8h-10h~~
  - ~~*Cours* : lire les transparents~~
    - ~~[7_intro_survol_NN](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/7_intro_survol_NN.pdf) : contient quelques éléments de présentation générale et de contexte, lecture rapide.~~
    - ~~[8_NN_Perceptron_MLP](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/8_NN_Perceptron_MLP.pdf), jusqu'à la page 19/26. Eléments de cours. Le détail des calculs de rétro-propagation du gradient peut être sauté.~~
  - ~~*Exercices* : [notebooks/8_NN_Perceptron_MLP](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks%2F8_NN_Perceptron_MLP)~~


####  ~~Travail à préparer pour la séance du lundi 06/4 8h-12h  (4h)~~
##### ~~8h-10h :~~

- ~~*Cours* : lire les transparents [6_Trees_RandomForest_Boosting](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/6_Trees_RandomForest_Boosting.pdf) JUSQU'AU  slide 18/36.~~
- ~~*Exercices* : [notebooks/6_classification_trees](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks%2F6_classification_trees), lire/exécuter/répondre aux questions des notebooks N1 et N2~~


##### ~~10h-12h :~~
- ~~*Cours* : lire les transparents [6_Trees_RandomForest_Boosting](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/6_Trees_RandomForest_Boosting.pdf)   slides 18/36 à 26/36~~
 - ~~*Exercices* : [notebooks/6_classification_trees](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks%2F6_classification_trees), lire/exécuter/répondre aux questions des notebooks N3~~





#### ~~Travail à préparer pour la séance du lundi 30/3 10h-12h (2h)~~

##### ~~4. Modèles linéaires et descente de gradient~~
 - ~~*Cours* : lire les transparents [4_linear_regression.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/4_linear_regression.pdf)~~
 - ~~*Exercices* : [notebooks/4_regression](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks%2F4_regression), lire/exécuter/répondre aux questions des notebooks~~
 - ~~*Quiz* : testez vos connaissances via ce
 [QCM](https://b.socrative.com/login/student/), *Room*: E3PARCOURSNUM1~~

 ##### ~~5.  Régularisation~~
  - ~~*Cours* : lire les transparents [5_linear_model_regularization.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/5_linear_model_regularization.pdf)~~
  - ~~*Exercices* : [notebooks/5_regularization](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks%2F5_regularization), lire/exécuter/répondre aux questions des notebooks~~
  - ~~*Quiz* : testez vos connaissances via ce
  [QCM](https://b.socrative.com/login/student/), *Room*: E3PARCOURSNUM2~~

#### ~~Travail à préparer pour la séance du lundi 23/3 8h-12h (4h)~~

##### ~~1. Introduction au machine learning~~
 - ~~*Cours* : lire les transparents [1_intro.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/1_intro.pdf)~~
 - ~~*Exercices* : [notebooks/1_introduction](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks%2F1_introduction), lire/exécuter/répondre aux questions des notebooks~~
 - ~~*Quiz* : testez vos connaissances via ce
 [QCM](https://b.socrative.com/login/student/), *Room*: E3PARCOURSNUM1~~

#####  ~~2. Premier algorithme : k plus proches voisins~~
 -  ~~*Cours* : lire les transparents [2_knn.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/2_knn.pdf)~~
 -  ~~*Exercices* : [notebooks/2_knn](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks%2F2_knn), lire/exécuter/répondre aux questions des notebooks~~
 -  ~~*Quiz* : testez vos connaissances via ce
 [QCM](https://b.socrative.com/login/student/), *Room*: E3PARCOURSNUM2~~

#####  ~~3. Validation et Sélection de modèles~~
-  ~~*Cours* : lire les transparents [3_model_assesment.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/3_model_assesment.pdf)~~
-  ~~*Exercices* : [notebooks/3_model_assesment](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks%2F3_model_assesment), lire/exécuter/répondre aux questions des notebooks~~
-  ~~*Quiz* : testez vos connaissances via ce [QCM](https://b.socrative.com/login/student/), *Room*: E3PARCOURSNUM3~~

#### Divers
- Les QCM des séances précédentes sont maintenant disponibles en version [texte/pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/quiz-pdf/)
- Pour ceux qui ne l'ont pas vu et que ca peut aider, il y a une  _intro sur python_ [notebooks/0_python_in_a_nutshell](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks%2F0_python_in_a_nutshell)

-----
-->


## Bienvenue dans le Parcours Numérique E3 !

Vous trouverez dans ce repo gitlab une
présentation de l'[UE Parcours Numérique](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/tree/master/UE-parcours-num) (resp. Stéphane Mocanu),
ainsi que le matériel nécessaire à l'enseignement de *Machine Learning* :
 - supports de cours ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/tree/master/slides))
 - exemples et exercices sous forme de [notebooks python](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks) (fichiers `.ipynb`) et/ou via des applis en ligne,
 <!-- - quiz (liens vers l'outil en ligne [Socrative](https://b.socrative.com/login/student/)) -->
 - données et descriptifs pour les projets (à venir)

Ces ressources seront actualisées en fonction de l'avancement des séances.

#### <a name="outils">Comment utiliser éditer/sauvegarder/exécuter vos notebooks python pour le projet ?</a>

Plusieurs outils ou ressources sont à votre disposition :
- Travailler sur votre pc en ayant installé la <a  href="https://www.anaconda.com/downloads">distribution Anaconda</a>.
- Utiliser le service `jupyterhub` de l'UGA, [jupyterhub.u-ga.fr](https://jupyterhub.u-ga.fr), afin de pouvoir exécuter vos notebooks sur le serveur de calcul de l'UGA tout en sauvegardant vos modifications et les résultats. Très utile pour lancer un calcul en tâche de fond (connexion avec votre compte Agalan ; nécessite de téléverser vos notebooks+données sur le serveur)
- Utiliser le service équivalent de google, [google-colab](https://colab.research.google.com/), qui permet d'exécuter/sauvegader vos notebooks et aussi de *partager l'édition à plusieurs collaborateurs* ce qui peut être utile pour votre projet en trinôme (nécessite un compte google et de téléverser vos notebooks+données dans votre Drive)

#### Comment utiliser les notebooks Python ?

Les exemples et exercices se feront sous python 3.x à travers [scikit-learn](https://scikit-learn.org/),  et également [tensorflow](https://www.tensorflow.org/).
Ce sont deux packages de machine learning parmi les plus utilisés actuellement.

Les *Jupyter Notebooks*  (fichiers `.ipynb`) sont des programmes contenant à la fois des cellules de code (pour nous Python)
et du texte en markdown pour le côté narratif.
Ces notebooks sont souvent utilisés pour explorer et analyser des données. Leur traitement se fait avec une application `jupyter-notebook`, ou `juypyter-lab`, à laquelle on accède par son navigateur web.

Afin de de pouvoir les exécuter vous avez deux possibilités :

1. Téléchargez les notebooks pour les exécuter sur votre machine. Cela requiert d'avoir installé un environnement Python (> 3.3), et les packages Jupyter notebook et scikit-learn. On recommande de les installer via la <a la href="https://www.anaconda.com/downloads">distribution Anaconda</a> qui installera directement toutes les dépendences nécessaires

**Ou**

2.  Utilisez un service en ligne *jupyterhub* :
 - on vous recommande celui de l'UGA [jupyterhub.u-ga.fr](https://jupyterhub.u-ga.fr) afin d'exécuter vos notebooks sur le serveur de cacul de l'UGA tout en sauvegardant votre code et les résultats (connexion avec vos identifiants Agalan ; il suffit ensuite de téléverser vos notebooks +  données sur le serveur). Pratique également pour lancer un calcul un peu long en tâche de fond
 - il existe des alternatives comme le service équivalent de google, [google-colab](https://colab.research.google.com/), qui permet d'exécuter/sauvegader vos notebooks et aussi de *partager l'édition à plusieurs collaborateurs* ce qui peut être utile pour votre projet (nécessite un compte google et de téléverser vos notebooks+données dans votre Drive)


**Ou**

3. Utilisez le service *mybinder* pour les exécuter de manière interactive et à distance :

     (ouvrir le lien fourni en en-tête de chaque notebook, puis attendre quelques secondes que l'environnement se charge). Attention : Binder est conçu pour un codage interactif mais *éphémère*, vos modificiations/codes/résultats seront perdus lorsque votre session expire (typiquement à la fermeture du navigateur ou après 10mn d'inactivité)


**Note :** Vous trouverez également parmi les notebooks une initiation à Python [notebooks/0_python_in_a_nutshell](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/tree/master/notebooks%2F0_python_in_a_nutshell)

<!-- #### Comment s'entrainer aux QCM ?

On utilise l'outil [Socrative](https://b.socrative.com/login/student/) pour les quiz/QCM. Pour y accéder :
  1. se connecter à l'outil en tant qu'étudiant  https://b.socrative.com/login/student/
  2. saisir la `ROOM` qu'on vous a indiquée (pex `E3PARCOURSNUM`)
  3. entrer votre nom et prénom (pas de pseudo : ces tests ne sont pas notés, mais ils permettent de vous suivre et de vérifier qu'il n'y a pas de problème)
  4. se déconnecter ('Log out') à la fin du quiz.

Vous pouvez refaire le quiz plusieurs fois si vous le souhaitez.
Ces quiz seront actualisés en fonction des séances.
-->
<!-- #### Comment contacter les profs, poser mes questions, ... ?

On a mis en place une messagerie instantanée (serveur Riot de l'ensimag) pour poser vos questions durant les séances, échanger des documents si besoin (on verra les usages au fil de l'eau)... Connexion au salon réservé pour le cours avec vos identifiants Agalan habituels à l'adresse :
- https://riot.ensimag.fr/#/room/#e3-numerique:ensimag.fr
-->

#### Remarques diverses sur les supports

- Les supports sont rédigés en anglais. Tous les outils ou packages de *machine learning* le sont également ! Vous serez donc familiarisé avec le vocabulaire, le *jargon* du domaine.
- Les slides sont conçus pour être autosuffisants (même si le coté narratif est souvent limité par le format)
- En complément des slides et des références bibliographiques/web, on vous propose en général des liens ou des vidéos (au début ou à la fin des slides) spécifiques aux notions présentées. Ces listes ne sont bien sûr pas exhaustives, et  vous trouverez à travers le web beaucoup de ressources souvent pédagogiques. N'hésitez pas à faire vos propres recherches et à les partager si vous les trouvez utiles.
